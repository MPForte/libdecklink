//
// Created by tibo on 12/01/18.
//

#pragma once

#include <vector>
#include <memory>

#include <boost/optional.hpp>
#include <boost/noncopyable.hpp>

#include "decklink_sdk/DeckLinkAPI.h"
#include "libdecklink/errors.hpp"
#include "libdecklink/types.hpp"
#include "libdecklink/decklink_handle_deleter.hpp"
#include "libdecklink/device_input_interface.hpp"
#include "libdecklink/notification_callback.hpp"

namespace DeckLink {
    
    class Device: private boost::noncopyable {
    
        friend class DeviceInputInterface;
        
    public: /* Static Methods */
        
        static std::vector<Device> List();
        
        static boost::optional<Device> Get(const std::string& name);
    
    public: /* Methods */
        
        /**
         * Default constructor.
         *
         * This constructs a dummy Device that cannot actually be used for anything. It just serves
         * to provide a default constructor.
         */
        Device() {}
        
        explicit Device(IDeckLink* device_ptr);
        
        Device(Device&& src) noexcept;
        
        Device& operator= (Device&& rhs) noexcept;
        
        virtual ~Device();
        
        // Configiuration methods
  
        // TODO : Trash optionals - we should always get a value. Unless maybe we swallow not implemented errors
        
        std::string get_model_name() const;
        std::string get_display_name() const;
        std::string get_vendor_name() const;
        
        std::string get_long_name() const;
        
        boost::optional<bool> get_bool_attribute(const AttributeID flag) const;
        void set_bool_attribute(const AttributeID flag, bool value);
    
        boost::optional<int64_t> get_int_attribute(const AttributeID flag) const;
        void set_int_attribute(const AttributeID flag, int value);
    
        boost::optional<double> get_float_attribute(const AttributeID flag) const;
        void  set_float_attribute(const AttributeID flag, float value);
    
        boost::optional<std::string> get_string_attribute(const AttributeID flag) const;
        void       set_string_attribute(const AttributeID flag, std::string value);
    
        boost::optional<bool> get_bool_configuration(const ConfigurationID flag) const;
        void set_bool_configuration(const ConfigurationID flag, bool value);
    
        boost::optional<int64_t> get_int_configuration(const ConfigurationID flag) const;
        void set_int_configuration(const ConfigurationID flag, int value);
    
        boost::optional<double> get_float_configuration(const ConfigurationID flag) const;
        void  set_float_configuration(const ConfigurationID flag, float value);
    
        boost::optional<std::string> get_string_configuration(const ConfigurationID flag) const;
        void       set_string_configuration(const ConfigurationID flag, std::string value);
    
        boost::optional<bool> get_bool_status(const StatusID flag) const;
        void set_bool_status(const StatusID flag, bool value);
    
        boost::optional<int64_t> get_int_status(const StatusID flag) const;
        void set_int_status(const StatusID flag, int value);
        
        // Important options - easy access
        
        bool supports_input_format_detection() const;
        
        // Notifications
        void set_notifications_callback(NotificationCallback&& cb);
        void clear_notification_callback();

        // Input / Output Interfaces
    
        DeviceInputInterface& input();
        
        // TODO - Add output interface methods
        
        // TODO - Add Keyer interface methods
        
        // Advanced stuff
        
        /**
         * Get a raw pointer to the underlying device.
         * This allows you to call additional methods that may not be wrapped by the Device class.
         *
         * @return A pointer to the device
         */
        IDeckLink* get_raw_device();

    protected: /* Methods */
        
        
        /**
         *
         * @note This function is marked const even though it really isn't. The \c _attributes_impl
         * interface is lazy loaded so every time we call a \c *_attributes method we have to check
         * if the interface has been loaded and if not load it.
         * Since we want the \c get_*_attribute methods to be const we need this method to be const.
         */
        void load_attributes_interface() const;
        void load_configuration_interface() const;
        void load_status_interface() const;
        void load_notification_interface() const;
    
    
        template <typename AttributeType>
        boost::optional<AttributeType> parse_configuration_query_result(
            HResult result,
            AttributeType attribute_value
        ) const;
        

    private: /* Member Variables */

        DeviceInputInterface _input_interface;
        
        bool _notification_callback_subscribed = false;
        NotificationCallback _notification_callback;
        
        mutable std::unique_ptr<IDeckLink, IDeckLinkHandleDeleter> _device_impl;
        mutable std::unique_ptr<IDeckLinkStatus, IDeckLinkHandleDeleter> _status_impl;
        mutable std::unique_ptr<IDeckLinkAttributes, IDeckLinkHandleDeleter> _attributes_impl;
        mutable std::unique_ptr<IDeckLinkConfiguration, IDeckLinkHandleDeleter> _configuration_impl;
        mutable std::unique_ptr<IDeckLinkNotification, IDeckLinkHandleDeleter> _notification_impl;
        
    };
    
    
    template <typename AttributeType>
    boost::optional<AttributeType> Device::parse_configuration_query_result(
        HResult result,
        AttributeType attribute_value
    ) const {
        switch (result) {
            case HResult::Ok:
                return attribute_value;
            
            case HResult::InvalidArg :
                return { };
    
            case HResult::NotImplemented:
                BOOST_THROW_EXCEPTION(decklink_driver_error()
                    << decklink_error_code(result)
                    << errmsg("The request is correct but not implemented on this hardware")
                );
            
            case HResult::Fail:
                BOOST_THROW_EXCEPTION(decklink_driver_error()
                    << decklink_error_code(result)
                    << errmsg("The query failed for an unknown reason")
                );
                
            default:
                BOOST_THROW_EXCEPTION(decklink_driver_error()
                    << decklink_error_code(result)
                    << errmsg("An unknown error occurred")
                );
        }
    }
    
}
