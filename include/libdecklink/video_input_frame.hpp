//
// Created by tibo on 30/01/18.
//

#pragma once

#include "libdecklink/video_frame.hpp"

namespace DeckLink {
    
    class VideoInputFrame : public VideoFrame {
    public:
        explicit VideoInputFrame(IDeckLinkVideoInputFrame* raw_input_frame);
    
        double stream_time() const;
        double hardware_reference_timestamp() const;
        
    protected:
    
    private:
        IDeckLinkVideoInputFrame* _raw_input_frame;
    };
    
}
