//
// Created by tibo on 30/01/18.
//

#include "libdecklink/errors.hpp"
#include "libdecklink/video_input_frame.hpp"

namespace DeckLink {
    
    VideoInputFrame::VideoInputFrame(IDeckLinkVideoInputFrame *raw_input_frame)
        : VideoFrame(static_cast<IDeckLinkVideoFrame*>(raw_input_frame))
        , _raw_input_frame(raw_input_frame)
    {
        // Do nothing
    }
    
    
    double VideoInputFrame::stream_time() const {
        BMDTimeValue frame_time, frame_duration;
        BMDTimeScale time_scale = 24000;
        const HRESULT result = _raw_input_frame->GetStreamTime(
            &frame_time, &frame_duration, time_scale);
        
        if (result != S_OK) {
            BOOST_THROW_EXCEPTION(decklink_driver_error()
                << errmsg("Unknown Error")
                << decklink_command("IDeckLinkVideoInputFrame::GetStreamTime")
                << decklink_error_code(static_cast<HResult>(result))
            );
        }
        
        return static_cast<double>(frame_time) / static_cast<double>(time_scale);
    }
    
    
    double VideoInputFrame::hardware_reference_timestamp() const {
        BMDTimeValue frame_time, frame_duration;
        BMDTimeScale time_scale = 24000;
        const HRESULT result = _raw_input_frame->GetHardwareReferenceTimestamp(
            time_scale, &frame_time, &frame_duration);
    
        if (result != S_OK) {
            BOOST_THROW_EXCEPTION(decklink_driver_error()
                << errmsg("Unknown Error")
                << decklink_command("IDeckLinkVideoInputFrame::GetStreamTime")
                << decklink_error_code(static_cast<HResult>(result))
            );
        }
    
        return static_cast<double>(frame_time) / static_cast<double>(time_scale);
    }
}
